

const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumazuada6ed/collections/";
const mLabAPIKey = "apiKey=8FjAtkP7hrqRkNYorpSJsknu8L4KIh5V";

function login(req, res) {
 console.log("POST /azurcebre/login");
 var email = req.body.email;
 var password = req.body.password;

 var query = 'q={"email": "' + email + '"}';

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("usuarios?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
         var response = {"msg" : "Usuario y/o password incorrecto"};
         res.status(404);
         res.send(response);
       } else if (!crypt.checkPassword(password, body[0].password)) {
            var response = {"msg" : "Usuario y/o password incorrecto"};
            res.status(404);
            res.send(response);
          } else {
            console.log("Got a user with that email and password, logging in");
            query = 'q={"id" : ' + body[0].id +'}';
            console.log("Query for put is " + query);
            var putBody = '{"$set":{"logged":true}}';
            httpClient.put("usuarios?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(errPUT, resMLabPUT, bodyPUT) {
                console.log("PUT done");
                var response = {
                  "msg" : "Usuario logado con éxito",
                  "idUsuario" : body[0].id,
                };
                res.send(response);
              }
          );
        }
      }
  );
}

function logout(req, res) {
 console.log("POST /azurcebre/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("usuarios?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {"msg" : "Logout incorrecto, usuario no encontrado"}
       res.status(404);
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("usuarios?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id,
             "first_name" : body[0].first_name,
             "last_name" : body[0].last_name,
           }
           res.send(response);
         }
       )
     }
   }
 );
}

module.exports.login = login;
module.exports.logout = logout;
