const io =require('../io');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumazuada6ed/collections/";
const mLabAPIKey = "apiKey=8FjAtkP7hrqRkNYorpSJsknu8L4KIh5V";

/*** Esta función accede al fichero, no al mLab
***
function getCuentasV1(req,res){
  console.log("GET /azurcebre/usuarios/:id/cuentas");
  var cuentas = require ('../cuentas.json');
  console.log('usuario'+ req.params.id);
  var cont = 0
  var result = [];
  for (cuenta of cuentas) {
  if(cuenta.id == req.params.id){
    console.log('cuenta IBAN:' + cuenta.IBAN);
    result[cont] = cuenta.IBAN;
    cont ++;
      }
    }
    res.send(result);
  }
*****/

function getCuentasById(req, res) {
  console.log("GET /azurcebre/usuarios/:id/cuentas");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log("Query es " + query);

  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("cuentas?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body){
       console.log(body);
       if (err){
         var response = {"msg" : "Error obteniendo cuentas"};
         res.status(500);
       } else{
         if (body.length > 0){
           var response = body;
           res.status(200);
         }else{
           var response = {"msg" : "El usuario no tiene cuentas."};
           res.status(404);
         }
       }
       res.send(response);
     }
   );
}


function createCuenta(req,res){
    console.log("POST /azurcebre/cuentas");
    console.log("id es : " + req.body.id);

    var num1 = Math.random() * 100 + 1;
    var num2 = Math.random() * 10000;
    var num3 = Math.random() * 10000;
    var num4 = Math.random() * 10000;
    var num5 = Math.random() * 10000;
    var num6 = Math.random() * 10000;
    var iban = "ES" + num1.toFixed(0) +
               " " + num2.toFixed(0) +
               " " + num3.toFixed(0) +
               " " + num4.toFixed(0) +
               " " + num5.toFixed(0) +
               " " + num6.toFixed(0);
    console.log(iban)

    var newCta = {
            "id" : req.body.id,
            "IBAN" : iban,
            "balance" : 1
          };

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado");
    httpClient.post("cuentas?" + mLabAPIKey, newCta,
          function(err,resMLab, body) {
              console.log("Cuenta creada con éxito");
//            res.send({"msg" : "Cuenta creada con éxito"})
          }
      );
}

function deleteCuenta(req, res){
    console.log("DELETE /azurcebre/cuentas/:iban");
    var iban = req.params.iban;
    var query = 'q={"IBAN": "' + iban + '"}';
    // tenía puesto sin doble comilla, pero tenía que ponerlas en el POSTMAN
    // var query = 'q={"IBAN": "' + iban + '"}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Client created");
    httpClient.get("cuentas?" + query + "&" + mLabAPIKey,
         function(err, resMLab, body){
           if (err){
             var response = {"msg" : "Error obteniendo cuentas"}
             res.status(500);
           } else{
             if (body.length > 0){
                console.log(body[0]._id.$oid);
                var id_mLab = body[0]._id.$oid;
                httpClient.del("cuentas/" + id_mLab + "?" + mLabAPIKey,
                  function(errdel, resdel, bodydel) {
                        return console.log(resdel.statusCode);
                    });
                var response = {"msg" : "Cuenta con iban " + iban + " Borrada"};
                res.status(200)
    	 	      } else{
                  var response = {"msg" : "Cuenta No encontrada"}
                  res.status(404);
             }
           }
           res.send(response);
         }
       );
    }


//module.exports.getCuentasV1= getCuentasV1;
module.exports.getCuentasById = getCuentasById;
module.exports.createCuenta = createCuenta;
module.exports.deleteCuenta = deleteCuenta;
