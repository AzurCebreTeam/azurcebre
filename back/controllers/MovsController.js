const io =require('../io');
var dateformat= require('dateformat');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumazuada6ed/collections/";
const mLabAPIKey = "apiKey=8FjAtkP7hrqRkNYorpSJsknu8L4KIh5V";

function getMovimientosV1(req,res){
  console.log('GET azurcebre/v1/cuenta/:id_cuenta/movimientos');
  var movimientos = require ('../movimientos.json');
  var resultado = [];
  cont = 0;
  console.log('IBAN'+ req.params.id_cuenta);
  for (mov of movimientos){
    if(mov.IBAN == req.params.id_cuenta){
      resultado[cont]= mov;
      cont++;
      }
      }
        res.send(resultado);
}

function getMovimientosbyCuenta(req,res){
  console.log('GET /azurcebre/v2/cuenta/:id_cuenta/movimientos');
  var id_cuenta = req.params.id_cuenta;
  var query = 'q={"IBAN":"'+ id_cuenta + '"}';
  var httpClient= requestJson.createClient(baseMLabURL);
  console.log('query:' + query);
  httpClient.get("movimientos?" + query + "&" + mLabAPIKey,
    function(err,resMlab,body){
      var response ={};
      if(err) {
        response = {"msg": "error en la BBDD"};
        res.status(500);
        } else {
        if(body.length>0){
          response = body;
          res.status(200);
          } else {
            response ={"msg":"no hay movimientos"}
            res.status(400);
          }
        }
        res.send(response);
      }
    );
  }

function createMovimientoV1(req,res){
  console.log('POST azurcebre/v1/altamovimientos/id_cuenta');
  var newMovs = {
    "IBAN": req.params.id_cuenta,
    "fecha": req.body.fecha,
    "tipo" : req.body.tipo,
    "cantidad": req.body.cantidad
    };
    var movs = require('../movimientos.json');
    var cuentas = require('../cuentas.json');
    movs.push(newMovs);
    io.WriteUserDatatoFile(movs);
    console.log('Cuenta:'+req.params.id_cuenta);
    res.send({"msg":"movimiento añadido con exito"});
  }

function createMovimientoByCuenta(req,res){
  console.log('POST azurcebre/v2/altamovimientos/id_cuenta');
  var day = dateformat(new Date(),"dd-mm-yyyy H:MM");
  var newMovs = {
    "IBAN": req.params.id_cuenta,
    "fecha": day,
    "tipo" : req.body.tipo,
    "cantidad": req.body.cantidad
  };
  console.log('fecha'+ day);
  var response ={};
  var httpClient =requestJson.createClient(baseMLabURL);
  httpClient.post("movimientos?" + mLabAPIKey, newMovs,
  function(err, resMLab, body) {
    console.log('Registrado el movimiento con éxito');
    console.log(err);
    if (!err ) {
     var balance_final = newMovs.cantidad;
     if (newMovs.tipo == "I" || newMovs.tipo == "R" ) {
     if (newMovs.tipo == "R") {balance_final = -newMovs.cantidad;}
     console.log('cantidad:'+ balance_final);
      var query = 'q={"IBAN":"'+ newMovs.IBAN + '"}';
      var httpClient= requestJson.createClient(baseMLabURL);
      console.log('query:' + query);
      httpClient.get("cuentas?" + query + "&" + mLabAPIKey,
       function(err3,resMlab3,body3){
         if(err3) {
           response = {"msg": "error en la BBDD"};
           res.status(500);
           } else {
           if(body3.length>0){
             balance_final= parseFloat(body3[0].balance)+parseFloat(balance_final);
             console.log('balance_final:'+balance_final);
             var set={"balance": balance_final};
             var putBody = '{"$set": {"balance":'+set.balance+'}}';
             console.log('putbody'+putBody);
             httpClient.put("cuentas?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(errPUT, resMLabPUT, bodyPUT) {
                if(errPUT) {
                  response = {"msg": "error en la BBDD"};
                  res.status(500);
                  } else {
                    var  response = {
                        "msg":"Balance actualizado con éxito",
                        "IBAN": body3[0].IBAN,
                        "balance": set.balance
                      }
                      console.log("Balance actualizado con exito"+balance_final);
                      res.status(200);
                      }
                res.send(response);
             }
           );
         }
       }
     }
     );
     }
    }
  }
  );
 }
module.exports.getMovimientosV1=getMovimientosV1;
module.exports.getMovimientosbyCuenta=getMovimientosbyCuenta;
module.exports.createMovimientoV1=createMovimientoV1;
module.exports.createMovimientoByCuenta=createMovimientoByCuenta;
