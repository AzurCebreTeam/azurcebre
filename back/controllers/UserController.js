
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumazuada6ed/collections/";
const mLabAPIKey = "apiKey=8FjAtkP7hrqRkNYorpSJsknu8L4KIh5V";


function getUsuarios(req, res) {
  console.log("GET /azurcebre/usuarios");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  httpClient.get("usuarios?" + mLabAPIKey,
     function(err, resMLab, body){
       var response = !err ? body : {
         "msg" : "Error obteniendo usuarios."
       }
       res.send(response);
     }
   );
}

function getUsuarioById(req, res) {
  console.log("GET /azurcebre/usuarios/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  httpClient.get("usuarios?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body){
       if (err){
         var response = {
           "msg" : "Error obteniendo usuario"
         }
         res.status(500);
       } else{
         if (body.length > 0){
           var response = body [0];
         }else{
           var response = {
             "msg" : "Usuario No encontrado"
           }
           res.status(404);
         }
       }
       res.send(response);
     }
   );
}

function createUsuario(requ,resu){
    console.log("POST /azurcebre/usuarios");
    console.log("first_name es : " + requ.body.first_name);
    console.log("last_name es : " + requ.body.last_name);
    console.log("email es : " + requ.body.email);
    console.log("phone es : " + requ.body.phone);

    var sort = 's={"id": -1}';
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Client created");
    httpClient.get("usuarios?" + sort + "&" + mLabAPIKey,
      function(err, resMLab, body){
        if (err){
          var response = {
            "msg" : "Error creación usuario"
          }
          res.status(500);
        } else{
          var ident = body [0].id + 1;
          var newUser = {
            "id" : ident,
            "first_name" : requ.body.first_name,
            "last_name" : requ.body.last_name,
            "email" : requ.body.email,
            "password" : crypt.hash(requ.body.password),
            "phone" : requ.body.phone
          };

          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("Client created");
          httpClient.post("usuarios?" + mLabAPIKey, newUser,
            function(err2,resMLab2, body2) {
              console.log("Usuario creado con éxito");
              const ctasController2 = require('./CtasController');
              var reqct = {"body": {"id":ident}};
              var resct = {};
              console.log(reqct.body.id);
              ctasController2.createCuenta(reqct, resct)
              console.log(" FIN ");
              resu.send({"msg" : "Usuario creado con éxito"});
                  }
              );
          }
        }
      );
}

function deleteUsuario(req, res){
    console.log("DELETE /azurcebre/usuarios/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  httpClient.get("usuarios?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body){
       if (err){
         var response = {
           "msg" : "Error obteniendo usuario"
         };
         res.status(500);
       } else{
         if (body.length > 0){
		         console.log(body[0]._id.$oid);
			        var id_mLab = body[0]._id.$oid;
                httpClient.del("usuarios/" + id_mLab + "?" + mLabAPIKey, function(errdel, resdel, bodydel) {
                return console.log(resdel.statusCode);
              });
		          var response = {
                  "msg" : "Usuario con id: " + id + " Borrado"
                }
	 	      }else{
              var response = {
                  "msg" : "Usuario No encontrado"
                };
              res.status(404);
         }
       }
       res.send(response);
     }
   );
}


module.exports.getUsuarios = getUsuarios;
module.exports.getUsuarioById = getUsuarioById;
module.exports.createUsuario = createUsuario;
module.exports.deleteUsuario = deleteUsuario;
