
const express = require ('express');
const app = express();
const port = process.env.PORT || 3000;

const bodyParser = require ('body-parser');
app.use(bodyParser.json());
app.listen(port);
console.log("API del proyecto escuchando en el puerto " + port);

const userController = require('./controllers/UserController');
const ctasController = require('./controllers/CtasController');
const movsController = require('./controllers/MovsController');
const authController = require('./controllers/AuthController');

app.get("/azurcebre/usuarios", userController.getUsuarios);
app.get("/azurcebre/usuarios/:id", userController.getUsuarioById);
app.post("/azurcebre/usuarios", userController.createUsuario);
app.delete("/azurcebre/usuarios/:id", userController.deleteUsuario);

app.get('/azurcebre/v1/cuenta/:id_cuenta/movimientos',movsController.getMovimientosV1);
app.get('/azurcebre/v2/cuenta/:id_cuenta/movimientos',movsController.getMovimientosbyCuenta);
app.post('/azurcebre/v1/altamovimientos/:id_cuenta', movsController.createMovimientoV1);
app.post('/azurcebre/v2/altamovimientos/:id_cuenta',movsController.createMovimientoByCuenta);

//app.get("/azurcebre/v1/usuario/:id/cuentas",ctasController.getCuentasV1);
app.get("/azurcebre/usuarios/:id/cuentas", ctasController.getCuentasById);
app.post("/azurcebre/cuentas", ctasController.createCuenta);
app.delete("/azurcebre/cuentas/:iban", ctasController.deleteCuenta);

app.post('/azurcebre/login', authController.login);
app.post('/azurcebre/logout/:id', authController.logout);
